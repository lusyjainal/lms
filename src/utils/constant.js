export const LIST = [
     {
        url: 'https://i.ibb.co/P1Nbh0W/landing-page.png',
        title: 'Padi UMKM (landinng page e-commerce)',
        desc: 'slicing dari ui/ux ke web, debuging, perbaikan jika ada bug, refactoring, code review',
        linkUrl: 'https://padiumkm.id/',
        stack: ['html', 'css', 'Next.js', 'tailwindcss', 'typescript']
    },
    {
        url: 'https://i.ibb.co/DLGZ4tG/admin.png',
        title: 'Padi UMKM (admin dashboard)',
        desc: 'slicing dari ui/ux ke web, debuging, perbaikan jika ada bug, refactoring, code review',
        linkUrl: null,
        stack: ['html', 'css', 'Next.js', 'tailwindcss', 'typescript']
    },
    {
        url: 'https://i.ibb.co/TvgxvNP/seller.png',
        title: 'Padi UMKM (seller dashboard)',
        desc: 'slicing dari ui/ux ke web, debuging, perbaikan jika ada bug, refactoring, code review',
        linkUrl: null,
        stack: ['html', 'css', 'Next.js', 'tailwindcss', 'typescript']
    },
    {
        url: 'https://i.ibb.co/WkhKKZb/generasi-maju.png',
        title: 'SGM by DANONE (web app)',
        desc: 'Sebagai seorang frontend developer, saya membuat tampilan portal website project susu generasi maju yang menarik dan user-friendly agar pengguna mudah mendapatkan informasi yang mereka butuhkan tentang produk susu generasi maju dari Danone dengan mudah dan nyaman.',
        linkUrl: 'https://www.generasimaju.co.id/resep-keluarga-maju',
        stack: ['html', 'scss', 'jquery', 'BEM', 'bootstrap']
    },
    {
        url: 'https://i.ibb.co/vvnF4rx/Jepretan-Layar-2023-02-23-pukul-17-54-05.png',
        title: 'Life by IFG (web app)',
        desc: 'sebuah portal website yang memberikan informasi lengkap mengenai produk asuransi IFG kepada pengguna. Saya membuat tampilan website Life.id user-friendly dan mudah dinavigasi agar pengunjung dapat dengan mudah menemukan informasi yang mereka butuhkan.',
        linkUrl: 'https://www.life.id/',
        stack: ['next.js', 'redux', 'tailwind', 'react-hooks', 'PWA', 'dll']
    },
    {
        url: 'https://i.ibb.co/n7bmsyQ/Jepretan-Layar-2023-02-23-pukul-20-27-20.png',
        title: 'sangfor (web app)',
        desc: 'Sangfor Technologies adalah vendor global terkemuka untuk solusi infrastruktur IT, yang mengkhususkan pada Cloud Computing & Network Security dengan berbagai macam produk & layanan termasuk Hyper-Converged Infrastructure, Virtual Desktop Infrastructure, Next-Generation Firewall, Internet Access Management, Endpoint Protection, Ransomware Protection, Managed Detection and Response, WAN Optimization, SD-WAN, dan masih banyak lagi.',
        linkUrl: 'https://www.sangfor.com/',
        stack: ['html', 'scss', 'alpin.js', 'BEM', 'bootstrap']
    },
    {
        url: 'https://i.ibb.co/s2MDzrX/Jepretan-Layar-2023-02-23-pukul-20-52-53.png',
        title: 'lontara app (web app)',
        desc: 'LONTARA adalah sebuah aplikasi yang dikembangkan oleh PT. Usaha Insan Aksara (PT. UI Aksara) yang memiliki visi untuk membangun ekosistem literasi masyarakat Indonesia dengan memanfaatkan teknologi informasi untuk menciptakan wadah beserta komunitas cinta buku.',
        linkUrl: 'https://www.lontara.co.id/',
        stack: ['react.js', 'scss', 'styled-components', 'ant design']
    },
    {
        url: 'https://i.ibb.co/yYHf401/Jepretan-Layar-2023-02-23-pukul-21-07-07.png',
        title: 'Codex powered by telkom (web app)',
        desc: 'CODEX adalah bagian dari Telkom DBT (Departemen Bisnis dan Teknologi), yang membantu Telkom Indonesia untuk mempermudah pekerjaan mereka dengan HR Tech Platform kami.',
        linkUrl: 'https://codex.works/',
        stack: ['next.js', 'scss', 'matrial-ui', 'redux', 'formik']
    },
    {
        url: 'https://i.ibb.co/sWNvbNt/Jepretan-Layar-2023-02-23-pukul-21-16-43.png',
        title: 'Blubird design system (web app)',
        desc: 'adalah sebuah design dydtem yang bertujuan membuat reusable componts',
        linkUrl: 'https://dev-ui.bluebird.id/?path=/docs/getting-started-installation--page',
        stack: ['react.js', 'scss', 'matrial-ui', 'storybook']
    },

    {
        url: 'https://i.ibb.co/C1fPwG6/Frame-266.png',
        title: 'Blubird dasboard (web app)',
        desc: 'berguna untuk mengecek transaksi',
        linkUrl: null,
        stack: ['react.js', 'redux', 'scss', 'matrial-ui', 'dll'],
    },
    {
        url: 'https://i.ibb.co/bBdNtV3/3.png',
        title: 'Celhum travel dashboard (web app)',
        desc: 'dashboard untuk CMS travel',
        linkUrl: null,
        stack: ['react.js', 'redux', 'scss', 'matrial-ui', 'dll']
    },
    {
        url: 'https://i.ibb.co/dmQCHjL/4.png',
        title: 'Lontara dashboard (web app)',
        desc: 'dasboard cms untuk publisher',
        linkUrl: null,
        stack: ['react.js', 'redux', 'scss', 'matrial-ui', 'dll']
    },


    {
        url: 'https://i.ibb.co/rdHpF8W/1.webp',
        title: 'Lontara (mobile app)',
        desc: 'LONTARA adalah sebuah aplikasi yang dikembangkan oleh PT. Usaha Insan Aksara (PT. UI Aksara) yang memiliki visi untuk membangun ekosistem literasi masyarakat Indonesia dengan memanfaatkan teknologi informasi untuk menciptakan wadah beserta komunitas cinta buku.',
        linkUrl: 'https://play.google.com/store/apps/details?id=app.lontara.android&hl=id&gl=US',
        stack: ['React-native', 'react-native-elements', 'redux']
    },
    {
        url: 'https://i.ibb.co/y4hvrds/2.webp',
        title: 'Fitaja',
        desc: 'FitAja! merupakan Digital Healthcare SuperApp dengan platform yang dikembangkan agar dapat menawarkan berbagai macam layanan dalam satu aplikasi',
        linkUrl: 'https://play.google.com/store/apps/details?id=com.mandiriinhealthmobile&hl=id&gl=US',
        stack: ['React-native', 'react-native-elements', 'redux']
    },
]

export const NAV_ITEMS = [
    {
        label: 'Beranda',
        href: '/',
    },
    {
        label: 'Portofolio',
        href: '/portofolio',
    }
];
