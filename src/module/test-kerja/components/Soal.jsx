import { Box, Container, Flex, List, ListItem, Popover, PopoverContent, PopoverTrigger, Stack, Text, useColorModeValue } from '@chakra-ui/react'
import Link from 'next/link'
import React from 'react'
import { Card } from '@/components';

export default function Soal() {

    return (
        <Container maxW={'7xl'}>
            <a
                target='_blank'
                href='https://docs.google.com/document/d/1UK33vMawdzOnIoyOk-WtUcYP-CrNKlRS7RqOQYuoerc/edit'
                style={{ textDecoration: 'underline' }}
            >Soal Aleph-Labs</a>
            <Text fontSize='md' mb={4} mt={4}>Jawaban:</Text>
            <Nomor1 />
            <Nomor2 />
            <Nomor3 />
        </Container>
    )
}

function Nomor1() {
    return (
        <>
            <p>1.</p>
            <Card />
        </>
    )
}


function Nomor2() {
    const linkColor = useColorModeValue('gray.600', 'gray.200');
    const linkHoverColor = useColorModeValue('gray.800', 'white');
    const popoverContentBgColor = useColorModeValue('white', 'gray.800');

    return (
        <>
            <Flex className='flex' mt={4}>
                <p>2. Arahkan cursor  untuk melihat hasil dropdown pada text <b>our works dibawah</b></p>
            </Flex>
            <Box ml={4} mb={4}>
                <Popover trigger={'hover'} placement={'bottom-start'}>
                    <PopoverTrigger>
                        <Link
                            p={2}
                            href={'#'}
                            fontSize={'md'}
                            fontWeight={500}
                            color={linkColor}
                            _hover={{
                                textDecoration: 'none',
                                color: linkHoverColor,
                            }}>
                            Our works
                        </Link>
                    </PopoverTrigger>

                    <PopoverContent
                        border={0}
                        boxShadow={'xl'}
                        bg={popoverContentBgColor}
                        p={4}
                        rounded={'xl'}
                        minW={'sm'}>
                        <Stack>
                            {[
                                'UI&UX Design',
                                'Web Development',
                                'Mobile Development'
                            ].map((child) => (
                                <Link href={'#'}>{child}</Link>
                            ))}
                        </Stack>
                    </PopoverContent>

                </Popover>
            </Box>
        </>
    )
}

function Nomor3() {
    return (
        <>
            <p>3. <b>Resep Telor Ceplok</b></p>
            <List spacing={3} ml={4} style={{ width: 300 }}>
                {[
                    'Telor ayam 1 butir',
                    'Minyak goreng 1/2 cup',
                    'Garam 1/2 sendok teh',
                    'Merica 1/2 sendok teh',
                    'Butter 1/2 sendok makan'].map((list, idx) => (
                        <ListItem style={{ backgroundColor: idx % 2 == 0 && '#EFEFEF', margin: 0 }}>
                            {list}
                        </ListItem>
                    ))}
            </List>
        </>
    )
}