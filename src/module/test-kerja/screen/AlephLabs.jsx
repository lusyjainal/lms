import { Footer, Navbar } from '@/components'
import React from 'react'
import Soal from '../components/Soal'

export default function AlephLabs() {
  return (
    <>
        <Navbar />
        <Soal/>
        <Footer />
    </>
  )
}
