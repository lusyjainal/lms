import { Footer, Navbar } from '@/components'
import React from 'react'
import ListPortofolio from '../components/ListPortofolio'

export default function MainPortofolio() {
  return (
    <>
        <Navbar />
        <ListPortofolio />
        <Footer />
    </>
  )
}
