import { LIST } from '@/utils/constant';
import {
    Box, Button, Container, Grid, Heading, HStack, Image, Link, Modal, ModalBody,
    ModalCloseButton, ModalContent, ModalFooter, ModalHeader, ModalOverlay, Tag, Text, useDisclosure, Wrap,
    WrapItem
} from '@chakra-ui/react';
import { useRouter } from 'next/router';
import { useState } from 'react';

const BlogTags = (props) => {
    return (
        <HStack spacing={2} marginTop={props.marginTop}>
            {props.tags.map((tag) => {
                return (
                    <Tag size={'md'} variant="solid" colorScheme="orange" key={tag}>
                        {tag}
                    </Tag>
                );
            })}
        </HStack>
    );
};

function BasicUsage({
    isOpen, url, onClose
}) {

    return (
        <>
            {/* <Button onClick={onOpen}>Open Modal</Button> */}

            <Modal isOpen={isOpen} onClose={onClose} size="xl">
                <ModalOverlay />
                <ModalContent>
                    <ModalCloseButton />
                    <ModalBody>
                        {/* <Lorem count={2} /> */}
                        <Image
                            transform="scale(1.0)"
                            src={url}
                            alt="some text"
                            objectFit="contain"
                            width="100%"
                            transition="0.3s ease-in-out"
                            _hover={{
                                transform: 'scale(1.05)',
                            }}
                        />
                    </ModalBody>

                    <ModalFooter>
                        <Button colorScheme='blue' mr={3} onClick={onClose}>
                            Close
                        </Button>
                    </ModalFooter>
                </ModalContent>
            </Modal>
        </>
    )
}

const ListPortofolio = () => {
    const [urlImage, setUrlImage] = useState('');
    const router = useRouter();
    const { isOpen, onOpen, onClose } = useDisclosure()

    return (
        <Container maxW={'7xl'}>
            <Grid templateColumns={{ base: 'repeat(1, 1fr)', md: 'repeat(3, 1fr)' }} gap={6}>
                <BasicUsage isOpen={isOpen} url={urlImage} onClose={onClose} />
                {LIST.map((item, idx) => (
                    <Wrap spacing="30px" marginTop="5" key={idx}>
                        <WrapItem>
                            <Box w="100%">
                                <Box borderRadius="lg" overflow="hidden" style={{ border: '2px solid #eee' }}>
                                    <Link

                                        textDecoration="none"
                                        _hover={{ textDecoration: 'none' }}
                                        onClick={() => {
                                            if (!item.linkUrl) {
                                                setUrlImage(item.url)
                                                onOpen();
                                                return
                                            }
                                            router.push(item.linkUrl)
                                        }}>
                                        <Image
                                            transform="scale(1.0)"
                                            src={item.url}
                                            alt="some text"
                                            objectFit="contain"
                                            width="100%"
                                            transition="0.3s ease-in-out"
                                            _hover={{
                                                transform: 'scale(1.05)',
                                            }}
                                        />
                                    </Link>
                                </Box>
                                <BlogTags tags={item.stack} marginTop="3" />
                                <Heading fontSize="xl" marginTop="2">
                                    <Link textDecoration="none" _hover={{ textDecoration: 'none' }}>
                                        {item.title}
                                    </Link>
                                </Heading>
                                <Text as="p" fontSize="md" marginTop="2">
                                    {item.desc}
                                </Text>
                                <Button mt={4} onClick={() => {
                                    if (!item.linkUrl) {
                                        setUrlImage(item.url)
                                        onOpen();
                                        return
                                    }
                                    router.push(item.linkUrl)
                                }}>lihat detail</Button>
                            </Box>
                        </WrapItem>
                    </Wrap>
                ))}
            </Grid>
        </Container>
    )
}

export default ListPortofolio