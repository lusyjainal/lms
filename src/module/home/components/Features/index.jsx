import { Box, Container, Flex, Icon, SimpleGrid, Stack, Text } from '@chakra-ui/react';
import { FcGallery, FcGlobe, FcTabletAndroid } from 'react-icons/fc';

const Feature = ({ title, text, icon }) => {
  return (
    <Stack>
      <Flex
        w={16}
        h={16}
        align={'center'}
        justify={'center'}
        color={'white'}
        rounded={'full'}
        bg={'gray.100'}
        mb={1}>
        {icon}
      </Flex>
      <Text fontWeight={600}>{title}</Text>
      <Text color={'gray.600'}>{text}</Text>
    </Stack>
  );
};

export default function SimpleThreeColumns() {
  return (
    <Container maxW={'7xl'}>
      <Box p={4}>
        <SimpleGrid columns={{ base: 1, md: 3 }} spacing={10}>
          <Feature
            icon={<Icon as={FcGlobe} w={10} h={10} />}
            title={'Frontend Web Developer'}
            text={
              'Building a frontend web page with several stacks including: HTML, CSS, JavaScript, React.js, Redux, Next.js, Material-UI, Chakra-UI, styled-component, Tailwind, React Hooks, and Formik, storybook.'
            }
            // text={
            //   'membangun sebuah halaman frontend web dengan beberapa stack yaitu: html, css, javascript, react.js, redux, next.js, matrial-ui, chakra-ui, tailwind, react-hooks, formik'
            // }
          />
          <Feature
            icon={<Icon as={FcTabletAndroid} w={10} h={10} />}
            title={'Frontend Mobile Developer'}
            text={
              'Building a frontend mobile page with several stacks including: React-native, React-Hooks, and Formik, react-native-elements.'
            }
          />
          <Feature
            icon={<Icon as={FcGallery} w={10} h={10} />}
            title={'Slicing UI/UX'}
            text={
              'I utilize my skills in slicing designs from Zeplin and Figma to create beautiful and responsive web and mobile pages. With proper slicing techniques, I am able to take designs and produce high-quality code that results in an amazing user experience.'
            }
          />
        </SimpleGrid>
      </Box>
    </Container>
  );
}