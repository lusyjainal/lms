import { Footer, Navbar } from '@/components'
import { Features, Hero } from '../../components'
import WrapBlog from '../../components/WrapBlog'

export default function HomeMain() {
  return (
    <>
      <Navbar />
      <Hero />
      <Features />
      <WrapBlog />
      <Footer />
    </>
  )
}
