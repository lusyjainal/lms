import { Blog } from '@/components'
import { Container, Heading } from '@chakra-ui/react'
import React from 'react'

export default function WrapBlog() {
    return (
        <Container maxW={'7xl'} marginTop={10}>
            <Heading as="h3">Blog terbaru</Heading>
            {[1, 2, 3, 4, 5].map(() => (
                <Blog />
            ))}
        </Container>
    )
}
