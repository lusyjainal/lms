import { Footer, Navbar } from '@/components'
import WrapBlog from '../../components/WrapBlog'

export default function BlogMain() {
  return (
    <>
      <Navbar />
      <WrapBlog />
      <Footer />
    </>
  )
}
